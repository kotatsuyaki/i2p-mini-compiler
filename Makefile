CCFLAGS=-O3 -Wall

.PHONY: all debug clean
all: main
	@cat input.txt | ./main

debug: CXXFLAGS += -DDEBUG -g -Og
debug: CCFLAGS += -DDEBUG -g -Og
debug: main

clean:
	rm -f main

main: main.c
	$(CC) $(CCFLAGS) main.c -o main
