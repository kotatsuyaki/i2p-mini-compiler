#include <ctype.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NO_EMPTY_REG SIZE_MAX

/* namespace vector */
typedef struct {
    size_t* ptr;
    size_t cap;
    size_t size;
} vector;

vector vector_new() {
    size_t* ptr = malloc(8 * sizeof(size_t));
    return (vector){ptr, 8, 0};
}

void vector_free(vector self) { free(self.ptr); }
size_t* vector_at(vector* self, size_t i) { return self->ptr + i; }
size_t* vector_back(vector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty vector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void vector_push(vector* self, size_t num) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(size_t) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = num;
    (self->size)++;
}

void vector_print(vector* self) {
    const size_t* base = self->ptr;
    for (size_t i = 0; i < self->size; i++) {
        fprintf(stderr, "%zu, ", *(base + i));
    }
    fputc('\n', stderr);
}

/* end namespace vector */
/* namespace boolvector */
typedef struct {
    bool* ptr;
    size_t cap;
    size_t size;
} boolvector;

boolvector boolvector_new() {
    bool* ptr = malloc(8 * sizeof(size_t));
    return (boolvector){ptr, 8, 0};
}

void boolvector_free(boolvector self) { free(self.ptr); }
bool* boolvector_at(boolvector* self, size_t i) { return self->ptr + i; }

bool* boolvector_back(boolvector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty boolvector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void boolvector_push(boolvector* self, bool value) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(bool) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = value;
    (self->size)++;
}

void boolvector_resize_and_clear(boolvector* self, size_t size) {
    self->ptr = realloc(self->ptr, sizeof(bool) * size);
    if (self->ptr == NULL) {
        fprintf(stderr, "Realloc failed");
        exit(1);
    }
    memset(self->ptr, false, sizeof(bool) * size);
    self->size = size;
    self->cap = size;
}

void boolvector_print(boolvector* self) {
    const bool* base = self->ptr;
    for (size_t i = 0; i < self->size; i++) {
        fprintf(stderr, "%d, ", *(base + i));
    }
    fputc('\n', stderr);
}

/* end namespace boolvector */
/* namespace string */
typedef struct {
    char* ptr;
    size_t cap;
    size_t size;
} string;
string string_new() {
    char* ptr = malloc(4 * sizeof(char));
    ptr[0]    = '\0';
    return (string){ptr, 4, 0};
}

string string_null() { return (string){NULL, 0, 0}; }
void string_free(string self) { free(self.ptr); }
char string_at(string* self, size_t i) { return *(self->ptr + i); }

void string_push(string* self, char ch) {
    if (self->size == self->cap - 2) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(char) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size]     = ch;
    self->ptr[self->size + 1] = '\0';
    (self->size)++;
}

bool string_eq(string* self, string* rhs) {
    if (self->size != rhs->size)
        return false;
    return strncmp(self->ptr, rhs->ptr, self->size) == 0;
}

/* end namespace string */
/* namespace Token */
typedef enum TokenType {
    TokenEnd,
    TokenAdd,
    TokenSub,
    TokenAnd,
    TokenOr,
    TokenXor,
    TokenMul,
    TokenDiv,
    TokenInt,
    TokenId,
    TokenAsgn,
    TokenLp,
    TokenRp,
    TokenUnknown
} TokenType;

char tokentype_op_char(TokenType type) {
    switch (type) {
    case TokenAdd:
        return '+';
    case TokenSub:
        return '-';
    case TokenMul:
        return '*';
    case TokenDiv:
        return '/';
    case TokenAnd:
        return '&';
    case TokenOr:
        return '|';
    case TokenXor:
        return '^';
    default: {
        fprintf(stderr, "Non-OP char");
        exit(1);
    }
    }
}

typedef struct {
    TokenType type;
    string data;
} Token;

Token token_new(TokenType type) {
    string data = string_new();
    return (Token){type, data};
}

Token token_new_op_from(char ch) {
    TokenType type;
    switch (ch) {
    case '+':
        type = TokenAdd;
        break;
    case '-':
        type = TokenSub;
        break;
    case '*':
        type = TokenMul;
        break;
    case '/':
        type = TokenDiv;
        break;
    case '&':
        type = TokenAnd;
        break;
    case '|':
        type = TokenOr;
        break;
    case '^':
        type = TokenXor;
        break;
    case '=':
        type = TokenAsgn;
        break;
    case '(':
        type = TokenLp;
        break;
    case ')':
        type = TokenRp;
        break;
    default:
        type = TokenUnknown;
    }
    return (Token){type, string_null()};
}

void token_push_char(Token* self, char ch) { string_push(&(self->data), ch); }

void token_print(const Token* self) {
    switch (self->type) {
    case TokenEnd:
        fprintf(stderr, "End");
        break;
    case TokenAdd:
        fprintf(stderr, "Add");
        break;
    case TokenSub:
        fprintf(stderr, "Sub");
        break;
    case TokenAnd:
        fprintf(stderr, "And");
        break;
    case TokenOr:
        fprintf(stderr, "Or");
        break;
    case TokenXor:
        fprintf(stderr, "Xor");
        break;
    case TokenMul:
        fprintf(stderr, "Mul");
        break;
    case TokenDiv:
        fprintf(stderr, "Div");
        break;
    case TokenInt:
        fprintf(stderr, "Int(%.*s)", (int)self->data.size, self->data.ptr);
        break;
    case TokenId:
        fprintf(stderr, "Id(%.*s)", (int)self->data.size, self->data.ptr);
        break;
    case TokenAsgn:
        fprintf(stderr, "Asgn");
        break;
    case TokenLp:
        fprintf(stderr, "Lp");
        break;
    case TokenRp:
        fprintf(stderr, "Rp");
        break;
    case TokenUnknown:
        fprintf(stderr, "Unknown");
        break;
    }
    if (self->type == TokenEnd)
        fputc('\n', stderr);
    else
        fputc(' ', stderr);
}

bool token_is_op(Token* self) {
    return (self->type == TokenAdd) || (self->type == TokenSub) ||
           (self->type == TokenAnd) || (self->type == TokenOr) ||
           (self->type == TokenXor);
}

/* end namespace Token */
/* namespace tokenvector */
typedef struct {
    Token* ptr;
    size_t cap;
    size_t size;
    size_t cur;
} tokenvector;

tokenvector tokenvector_new() {
    Token* ptr = malloc(16 * sizeof(Token));
    return (tokenvector){ptr, 16, 0, 0};
}

void tokenvector_free(tokenvector self) { free(self.ptr); }
Token tokenvector_at(tokenvector* self, size_t i) { return *(self->ptr + i); }
void tokenvector_adv(tokenvector* self) { (self->cur)++; }

Token* tokenvector_cur(tokenvector* self) {
    if (self->cur > self->size)
        return NULL;
    return self->ptr + self->cur;
}

Token* tokenvector_back(tokenvector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty vector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void tokenvector_push(tokenvector* self, Token token) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(Token) * (self->cap));
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = token;
    (self->size)++;
}

void tokenvector_print(tokenvector* self) {
    const Token* base = self->ptr;
    for (size_t i = 0; i < self->size; i++) {
        token_print(base + i);
    }
}

/* end namespace tokenvector */
/* namespace lex */
bool is_op(char ch) {
    return (ch == '+') || (ch == '-') || (ch == '*') || (ch == '/') ||
           (ch == '&') || (ch == '|') || (ch == '^') || (ch == '=') ||
           (ch == '(') || (ch == ')');
}

tokenvector lex() {
    tokenvector v = tokenvector_new();
    bool id_flag  = false;
    bool int_flag = false;
    int input;
    while ((input = getchar()) != EOF) {
        char ch = (char)input;
        if (ch == '\n') {
            tokenvector_push(&v, token_new(TokenEnd));
            id_flag = int_flag = false;
        } else if (isspace(ch)) {
            id_flag = int_flag = false;
        } else if (isalpha(ch)) {
            if (id_flag) {
                token_push_char(tokenvector_back(&v), ch);
            } else {
                tokenvector_push(&v, token_new(TokenId));
                token_push_char(tokenvector_back(&v), ch);
            }
            id_flag  = true;
            int_flag = false;
        } else if (isdigit(ch)) {
            if (int_flag) {
                token_push_char(tokenvector_back(&v), ch);
            } else {
                tokenvector_push(&v, token_new(TokenInt));
                token_push_char(tokenvector_back(&v), ch);
            }
            id_flag  = false;
            int_flag = true;
        } else if (is_op(ch)) {
            tokenvector_push(&v, token_new_op_from(ch));
            id_flag = int_flag = false;
        } else {
            fprintf(stderr, "Unknown char code %d\n", (int)ch);
            exit(1);
        }
    }
    return v;
}

/* end namespace lex */
/* namespace ast */
/* namespace ast::symtbl */
static string symtbl[1000];
void symtbl_init() {
    symtbl[0] = string_new();
    string_push(symtbl + 0, 'x');
    symtbl[1] = string_new();
    string_push(symtbl + 1, 'y');
    symtbl[2] = string_new();
    string_push(symtbl + 2, 'z');
}

typedef struct {
    size_t value;
    bool first_occur;
} SymtblGetResult;
SymtblGetResult symtbl_get(string* str) {
    size_t i = 0;
    while (symtbl[i].ptr != NULL) {
        if (string_eq(&symtbl[i], str))
            return (SymtblGetResult){ i, false };
        i++;
    }
    symtbl[i] = *str;
    return (SymtblGetResult){ i, true };
}

/* end namespace ast::symtbl */
typedef enum ASTType {
    ASTOp,
    ASTAsgn,
    ASTId,
    ASTInt,
} ASTType;
typedef struct _ASTNode {
    struct _ASTNode* left;
    struct _ASTNode* right;
    ASTType type;
    int32_t data;
} ASTNode;
typedef struct {
    ASTNode* root;
    tokenvector* tkv;
    bool legal;
    int32_t writes;
    vector reads;
    bool useless;
} AST;
ASTNode* astnode_new(ASTType type, int32_t data) {
    ASTNode* node = malloc(sizeof(ASTNode));
    (*node)       = (ASTNode){NULL, NULL, type, data};
    return node;
}

void astnode_free(ASTNode* self) {
    if (self->left)
        astnode_free(self->left);
    if (self->right)
        astnode_free(self->right);
    free(self);
}

void astnode_print(ASTNode* self) {
    if (self == NULL)
        return;
    if (self->type == ASTAsgn) {
        fprintf(stderr, "Asgn(");
        astnode_print(self->left);
        fprintf(stderr, ", ");
        astnode_print(self->right);
        fprintf(stderr, ")\n");
    } else if (self->type == ASTOp) {
        fprintf(stderr, "%c(", (char)(self->data));
        astnode_print(self->left);
        fprintf(stderr, ", ");
        astnode_print(self->right);
        fprintf(stderr, ")");
    } else if (self->type == ASTId) {
        fprintf(stderr, "Id[%" PRId32 "]", self->data);
    } else if (self->type == ASTInt) {
        fprintf(stderr, "Int[%" PRId32 "]", self->data);
    }
}

/* namespace ast::const_folding */
static bool consttbl[1000];
static size_t constvaltbl[1000];

typedef struct {
    bool is_legal;
    size_t value;
} ASTNodeOpEvalResult;

ASTNodeOpEvalResult astnode_op_eval(ASTNode* self) {
    if (self->data == (int32_t)'+')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data + self->right->data};
    else if (self->data == (int32_t)'-')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data - self->right->data};
    else if (self->data == (int32_t)'*')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data * self->right->data};
    else if (self->data == (int32_t)'/') {
        if (self->right->data == 0)
            return (ASTNodeOpEvalResult){false, 0};
        else
            return (ASTNodeOpEvalResult){true,
                                         self->left->data / self->right->data};
    } else if (self->data == (int32_t)'&')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data & self->right->data};
    else if (self->data == (int32_t)'|')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data | self->right->data};
    else if (self->data == (int32_t)'^')
        return (ASTNodeOpEvalResult){true,
                                     self->left->data ^ self->right->data};
    fprintf(stderr, "Unknown ASTNode operator data: %" PRId32 "\n", self->data);
    exit(1);
}

void astnode_constfold(ASTNode** p_self) {
    ASTNode* self = *p_self;
    if (!self)
        return;
    if (self->type == ASTOp) {
        astnode_constfold(&(self->left));
        astnode_constfold(&(self->right));

        if ((self->left->type == ASTInt) && (self->right->type == ASTInt)) {
            ASTNodeOpEvalResult res = astnode_op_eval(self);
            if (res.is_legal) {
                ASTNode* const_node = astnode_new(ASTInt, res.value);
                (*p_self)           = const_node;
                astnode_free(self);
            } else {
                printf("EXIT 1\n");
                exit(0);
            }
        }
    } else if (self->type == ASTId) {
        if (consttbl[self->data] == true) {
            ASTNode* const_node = astnode_new(ASTInt, constvaltbl[self->data]);
            (*p_self)           = const_node;
            astnode_free(self);
        }
    }
}

void ast_constfold(AST* self) {
    if (!(self->root))
        return;
    ASTNode* root = self->root;
    astnode_constfold(&(self->root->right));
    if (root->right->type == ASTInt) {
        consttbl[root->left->data]    = true;
        constvaltbl[root->left->data] = root->right->data;
    } else {
        consttbl[root->data] = false;
    }
}

/* end namespace ast::const_folding */
bool ast_expr(AST* self, ASTNode** node);

bool ast_fact(AST* self, ASTNode** node) {
    Token* tk = tokenvector_cur(self->tkv);
    tokenvector_adv(self->tkv);
    if (tk == NULL) {
        fprintf(stderr, "Unexpected end of token stream\n");
        return false;
    }
    if (tk->type == TokenAdd) {
        if (!(ast_fact(self, node)))
            return false;
    } else if (tk->type == TokenSub) {
        (*node)       = astnode_new(ASTOp, tokentype_op_char(tk->type));
        (*node)->left = astnode_new(ASTInt, 0);
        if (!(ast_fact(self, &((*node)->right))))
            return false;
    } else if (tk->type == TokenInt) {
        (*node) = astnode_new(ASTInt, atoi(tk->data.ptr));
    } else if (tk->type == TokenId) {
        SymtblGetResult res = symtbl_get(&( tk->data ));
        if (res.first_occur) {
            printf("EXIT 1\n");
            exit(0);
        }
        (*node) = astnode_new(ASTId, symtbl_get(&(tk->data)).value);
    } else if (tk->type == TokenLp) {
        ast_expr(self, node);
        Token* rptk = tokenvector_cur(self->tkv);
        tokenvector_adv(self->tkv);
        if (rptk == NULL) {
            fprintf(stderr, "Unexpected end of token stream\n");
            return false;
        }
        if (rptk->type != TokenRp) {
            fprintf(stderr, "Expected Rp\n");
            return false;
        }
    } else {
        fprintf(stderr, "Expected Add, Sub, Int, or Id\n");
        return false;
    }
    return true;
}

bool ast_term_tail(AST* self, ASTNode** parent) {
    Token* tk = tokenvector_cur(self->tkv);
    if (tk == NULL) {
        fprintf(stderr, "Unexpected end of token stream\n");
        return false;
    }
    if ((tk->type == TokenMul) || (tk->type == TokenDiv)) {
        tokenvector_adv(self->tkv);
        ASTNode* saved_parent = *parent;
        (*parent) = astnode_new(ASTOp, (size_t)tokentype_op_char(tk->type));
        (*parent)->left = saved_parent;
        if (!(ast_fact(self, &((*parent)->right))))
            return false;
        if (!(ast_term_tail(self, parent)))
            return false;
    }
    return true;
}

bool ast_term(AST* self, ASTNode** node) {
    ASTNode* fact = NULL;
    if (!ast_fact(self, &fact))
        return false;
    if (!(ast_term_tail(self, &fact)))
        return false;
    (*node) = fact;
    return true;
}

bool ast_expr_tail(AST* self, ASTNode** parent) {
    Token* tk = tokenvector_cur(self->tkv);
    if (tk == NULL) {
        fprintf(stderr, "Unexpected end of token stream\n");
        return false;
    }
    if (token_is_op(tk)) {
        tokenvector_adv(self->tkv);
        ASTNode* saved_parent = *parent;
        (*parent) = astnode_new(ASTOp, (size_t)tokentype_op_char(tk->type));
        (*parent)->left = saved_parent;
        if (!(ast_term(self, &((*parent)->right))))
            return false;
        if (!(ast_expr_tail(self, parent)))
            return false;
    }
    return true;
}

bool ast_expr(AST* self, ASTNode** node) {
    ASTNode* term = NULL;
    if (!ast_term(self, &term))
        return false;
    if (!(ast_expr_tail(self, &term)))
        return false;
    (*node) = term;
    return true;
}

void astnode_update_rw_info(ASTNode* self, AST* ast) {
    if (self->type == ASTAsgn) {
        ast->writes = self->left->data;
        fprintf(stderr, "AST writes to %" PRId32 "\n", ast->writes);
        astnode_update_rw_info(self->right, ast);
    } else if (self->type == ASTOp) {
        astnode_update_rw_info(self->left, ast);
        astnode_update_rw_info(self->right, ast);
    } else if (self->type == ASTId) {
        vector_push(&( ast->reads ), self->data);
    }
}

void ast_update_rw_info(AST* self) {
    if (self->root == NULL)
        return;
    astnode_update_rw_info(self->root, self);
}

bool ast_build(AST* self, ASTNode** node) {
    Token* tk1 = tokenvector_cur(self->tkv);
    if (tk1 == NULL) {
        fprintf(stderr, "Unexpected end of token stream\n");
        return false;
    }
    if (tk1->type == TokenEnd) {
        (self->tkv->cur)++;
    } else {
        tokenvector_adv(self->tkv);
        Token* tk2 = tokenvector_cur(self->tkv);
        tokenvector_adv(self->tkv);
        Token* tk3 = tokenvector_cur(self->tkv);
        if ((tk2 == NULL) || (tk3 == NULL)) {
            fprintf(stderr, "Unexpected end of token stream\n");
            return false;
        }
        if (tk1->type != TokenId) {
            fprintf(stderr, "Expected identifier\n");
            return false;
        }
        if (tk2->type != TokenAsgn) {
            fprintf(stderr, "Expected assignment operator\n");
            return false;
        }
        (*node)       = astnode_new(ASTAsgn, 0);
        (*node)->left = astnode_new(ASTId, symtbl_get(&(tk1->data)).value);
        if (!(ast_expr(self, &((*node)->right))))
            return false;
        Token* tkend = tokenvector_cur(self->tkv);
        if (tkend == NULL) {
            fprintf(stderr, "Unexpected end of token stream\n");
            return false;
        }
        if (tkend->type != TokenEnd) {
            fprintf(stderr, "Expected end of line, but got ");
            token_print(tkend);
            fputc('\n', stderr);
            return false;
        } else {
            tokenvector_adv(self->tkv);
        }
    }
    return true;
}

AST ast_new(tokenvector* tkv) {
    AST ast        = {NULL, tkv, true, 0, vector_new(), false};
    bool build_res = ast_build(&ast, &(ast.root));
    if (!build_res) {
        fprintf(stderr, "Build failed\n");
        ast.legal = false;
    }
    return ast;
}

void ast_print(AST* self) {
    if (self->root == NULL)
        return;
    astnode_print(self->root);
}

/* namespace astvector */
typedef struct {
    AST* ptr;
    size_t cap;
    size_t size;
    size_t cur;
} astvector;

astvector astvector_new() {
    AST* ptr = malloc(8 * sizeof(AST));
    return (astvector){ptr, 8, 0, 0};
}

void astvector_free(astvector self) { free(self.ptr); }
AST* astvector_at(astvector* self, size_t i) { return self->ptr + i; }

AST* astvector_back(astvector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty vector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void astvector_constfold(astvector* self) {
    for (size_t i = 0; i < self->size; i++) {
        ast_constfold(self->ptr + i);
    }
}

void astvector_deadcode_elim(astvector* self) {
    for (size_t i = 0; i < self->size; i++)
        ast_update_rw_info(self->ptr + i);

    bool read_table[64] = { false };
    read_table[0] = true;
    read_table[1] = true;
    read_table[2] = true;

    for (size_t i = self->size - 1; i-- > 0; ) {
        AST* ast = self->ptr + i;
        if (ast->root == NULL)
            continue;

        fprintf(stderr, "Attempting to eliminate AST: ");
        ast_print(ast);

        if (read_table[ast->writes] == false) {
            fprintf(stderr, "Found useless AST #%zu, marking it as useless.\n", i);
            ast->useless = true;
            continue;
        } else {
            fprintf(stderr, "Clear flag for %" PRId32 "\n", ast->writes);
            read_table[ast->writes] = false;
        }

        for (size_t j = 0; j < ast->reads.size; j++) {
            size_t read = *vector_at(&(ast->reads), j);
            read_table[read] = true;
            fprintf(stderr, "Raise flag for %zu\n", read);
        }
    }
}

void astvector_push(astvector* self, AST ast) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(AST) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = ast;
    (self->size)++;
}

/* end namespace astvector */
/* end namespace ast */
/* namespace idmap */
static vector idmap;
static size_t idmap_next;
static size_t idmap_native_name_count;
static vector curid_life_start;
static vector curid_life_end;
static boolvector spilled;

void idmap_print();
void idmap_init() {
    idmap            = vector_new();
    curid_life_start = vector_new();
    curid_life_end   = vector_new();
    spilled          = boolvector_new();
    size_t i         = 0;
    while (symtbl[i].ptr != NULL) {
        vector_push(&idmap, i);
        vector_push(&curid_life_start, 0);
        vector_push(&curid_life_end, SIZE_MAX);
        boolvector_push(&spilled, false);
        i++;
    }
    idmap_native_name_count = i;
    idmap_next              = i;
    idmap_print();
}

size_t idmap_id_to_cur(size_t id) { return idmap.ptr[id]; }
size_t idmap_new_temp(size_t ir_idx) {
    size_t new_curid = idmap_next++;
    vector_push(&idmap, new_curid);
    vector_push(&curid_life_start, ir_idx);
    vector_push(&curid_life_end, ir_idx);
    boolvector_push(&spilled, false);
    fprintf(stderr, "New temporary with CURID %zu\n", new_curid);
    return new_curid;
}

void idmap_print() {
    fprintf(stderr, "> idmap: ");
    vector_print(&idmap);
    fprintf(stderr, "> lifetime start: ");
    vector_print(&curid_life_start);
    fprintf(stderr, "> lifetime end: ");
    vector_print(&curid_life_end);
}

size_t idmap_remap(size_t id, size_t ir_idx) {
    size_t old_curid                       = idmap.ptr[id];
    *vector_at(&curid_life_end, old_curid) = ir_idx;
    size_t new_curid                       = idmap_next++;
    vector_push(&curid_life_start, ir_idx);
    vector_push(&curid_life_end, ir_idx);
    boolvector_push(&spilled, false);
    idmap.ptr[id] = new_curid;
    fprintf(stderr, "Remap ID %zu: CURID Old(%zu) => New(%zu)\n", id, old_curid,
            new_curid);
    return new_curid;
}

void idmap_extend_lifetime(size_t cur_id, size_t ir_idx) {
    size_t* life_end = vector_at(&curid_life_end, cur_id);
    if (*life_end == SIZE_MAX)
        *life_end = 0;
    if (*life_end < ir_idx)
        *life_end = ir_idx;
}

/* end namespace idmap */
/* namespace ir */
typedef enum IRType {
    IRSet,
    IRAdd,
    IRSub,
    IRMul,
    IRDiv,
    IRAnd,
    IROr,
    IRXor,
    IRMov,
    IRSave,
} IRType;

typedef struct {
    IRType type;
    int32_t dest;
    int32_t sc1;
    int32_t sc2;
} IR;

IR ir_new(IRType type, int32_t dest, int32_t sc1, int32_t sc2) {
    return (IR){type, dest, sc1, sc2};
}

size_t ir_get_sc_count(const IR* self) {
    IRType type = self->type;
    if ((IRAdd == type) || (IRSub == type) || (IRMul == type) ||
        (IRDiv == type) || (IRAnd == type) || (IROr == type) || (IRXor == type))
        return 2;
    if (IRSet == type)
        return 0;
    return 1;
}

void ir_print(const IR* self) {
    switch (self->type) {
    case IRSet:
        fprintf(stderr, "Set(%" PRId32 ", %" PRId32 ")\n", self->dest,
                self->sc1);
        break;
    case IRAdd:
        fprintf(stderr, "Add(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRSub:
        fprintf(stderr, "Sub(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRMul:
        fprintf(stderr, "Mul(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRDiv:
        fprintf(stderr, "Div(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRAnd:
        fprintf(stderr, "And(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IROr:
        fprintf(stderr, "Or(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRXor:
        fprintf(stderr, "Xor(%" PRId32 ", %" PRId32 ", %" PRId32 ")\n",
                self->dest, self->sc1, self->sc2);
        break;
    case IRMov:
        fprintf(stderr, "Mov(%" PRId32 ", %" PRId32 ")\n", self->dest,
                self->sc1);
        break;
    case IRSave:
        fprintf(stderr, "Save(%" PRId32 ", %" PRId32 ")\n", self->dest,
                self->sc1);
    }
}

/* namespace irvector */
typedef struct {
    IR* ptr;
    size_t cap;
    size_t size;
} irvector;

irvector irvector_new() {
    IR* ptr = malloc(16 * sizeof(IR));
    return (irvector){ptr, 16, 0};
}

void irvector_free(irvector self) { free(self.ptr); }
IR irvector_at(irvector* self, size_t i) { return *(self->ptr + i); }

IR* irvector_back(irvector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty vector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void irvector_push(irvector* self, IR ir) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(IR) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = ir;
    (self->size)++;
}

void irvector_print(irvector* self) {
    const IR* base = self->ptr;
    for (size_t i = 0; i < self->size; i++) {
        ir_print(base + i);
    }
}

/* end namespace irvector */
size_t irvector_push_from_astnode(irvector* self, ASTNode* node) {
    if (node->type == ASTOp) {
        ASTNode* lhs     = node->left;
        ASTNode* rhs     = node->right;
        size_t lhs_curid = irvector_push_from_astnode(self, node->left);
        size_t rhs_curid = irvector_push_from_astnode(self, node->right);
        IRType op_type;
        if (node->data == (size_t)'+')
            op_type = IRAdd;
        else if (node->data == (size_t)'-')
            op_type = IRSub;
        else if (node->data == (size_t)'*')
            op_type = IRMul;
        else if (node->data == (size_t)'/')
            op_type = IRDiv;
        else if (node->data == (size_t)'&')
            op_type = IRAnd;
        else if (node->data == (size_t)'|')
            op_type = IROr;
        else if (node->data == (size_t)'^')
            op_type = IRXor;
        else {
            fprintf(stderr, "Unknown operator type\n");
            exit(1);
        }
        size_t temp_curid = idmap_new_temp(self->size);
        idmap_extend_lifetime(lhs_curid, self->size);
        idmap_extend_lifetime(rhs_curid, self->size);
        irvector_push(self, ir_new(op_type, temp_curid, lhs_curid, rhs_curid));
        return temp_curid;
    } else if (node->type == ASTAsgn) {
        ASTNode* lhs              = node->left;
        ASTNode* rhs              = node->right;
        size_t lhs_curid          = irvector_push_from_astnode(self, lhs);
        size_t rhs_curid          = irvector_push_from_astnode(self, rhs);
        size_t lhs_remapped_curid = idmap_remap(lhs->data, self->size);
        idmap_extend_lifetime(lhs_curid, self->size);
        idmap_extend_lifetime(rhs_curid, self->size);
        irvector_push(self, ir_new(IRMov, lhs_remapped_curid, rhs_curid, 0));
        return 0;
    } else if (node->type == ASTId) {
        return idmap_id_to_cur(node->data);
    } else if (node->type == ASTInt) {
        size_t temp_curid = idmap_new_temp(self->size);
        irvector_push(self, ir_new(IRSet, temp_curid, node->data, 0));
        return temp_curid;
    } else {
        fprintf(stderr, "Unknown AST Node type\n");
        exit(1);
    }
}

irvector irvector_from_astvector(astvector* ast_vec) {
    irvector ir_vec = irvector_new();
    for (size_t i = 0; i < ast_vec->size; i++) {
        AST* ast = astvector_at(ast_vec, i);
        if (!(ast->root))
            continue;
        if (ast->useless)
            continue;
        irvector_push_from_astnode(&ir_vec, ast->root);
        idmap_print();
    }
    idmap_extend_lifetime(idmap_id_to_cur(0), ir_vec.size);
    irvector_push(&ir_vec, ir_new(IRSave, 0, idmap_id_to_cur(0), 0));
    idmap_extend_lifetime(idmap_id_to_cur(1), ir_vec.size);
    irvector_push(&ir_vec, ir_new(IRSave, 1, idmap_id_to_cur(1), 0));
    idmap_extend_lifetime(idmap_id_to_cur(2), ir_vec.size);
    irvector_push(&ir_vec, ir_new(IRSave, 2, idmap_id_to_cur(2), 0));
    return ir_vec;
}

/* end namespace ir */
/* namespace ins */
typedef enum InsType {
    MovRR, MovRC, MovRM, MovMR, Add, Sub, Mul, Div, And, Or, Xor, Exit
} InsType;

typedef struct {
    InsType type;
    int32_t dest;
    int32_t source;
    bool useless;
} Ins;

Ins ins_new(InsType type, int32_t dest, int32_t source) {
    return (Ins){ type, dest, source, false };
}

void ins_print(Ins* self, FILE* fd) {
    switch (self->type) {
    case MovRR:
        fprintf(fd, "MOV r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case MovRC:
        fprintf(fd, "MOV r%" PRId32 " %" PRId32 "\n", self->dest, self->source);
        break;
    case MovRM:
        fprintf(fd, "MOV r%" PRId32 " [%" PRId32 "]\n", self->dest, self->source);
        break;
    case MovMR:
        fprintf(fd, "MOV [%" PRId32 "] r%" PRId32 "\n", self->dest, self->source);
        break;
    case Add:
        fprintf(fd, "ADD r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Sub:
        fprintf(fd, "SUB r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Mul:
        fprintf(fd, "MUL r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Div:
        fprintf(fd, "DIV r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case And:
        fprintf(fd, "AND r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Or:
        fprintf(fd, "OR r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Xor:
        fprintf(fd, "XOR r%" PRId32 " r%" PRId32 "\n", self->dest, self->source);
        break;
    case Exit:
        fprintf(fd, "EXIT %" PRId32 "\n", self->dest);
        break;
    }
}

/* namespace insvector */
typedef struct {
    Ins* ptr;
    size_t cap;
    size_t size;
} insvector;
static insvector instructions;

insvector insvector_new() {
    Ins* ptr = malloc(8 * sizeof(Ins));
    return (insvector){ptr, 8, 0};
}

void insvector_free(insvector self) { free(self.ptr); }
Ins* insvector_at(insvector* self, size_t i) { return self->ptr + i; }

Ins* insvector_back(insvector* self) {
    if (self->size == 0) {
        fprintf(stderr, "Attempt to get back of empty insvector");
        exit(1);
    }
    return self->ptr + (self->size - 1);
}

void insvector_push(insvector* self, Ins ins) {
    if (self->size == self->cap - 1) {
        (self->cap) *= 2;
        self->ptr = realloc(self->ptr, sizeof(Ins) * self->cap);
        if (self->ptr == NULL) {
            fprintf(stderr, "Realloc failed");
            exit(1);
        }
    }
    self->ptr[self->size] = ins;
    (self->size)++;
}

void insvector_peephole_opt_post(insvector* self) {
    Ins* ins_end = self->ptr + self->size;
    Ins* ins_cur = self->ptr;
    Ins* ins_next = ins_cur;
    while (true) {
        ins_cur = ins_next;
        ins_next++;

        while ((ins_next < ins_end) && (ins_next->useless == true)) {
            ins_next++;
        }
        if (ins_next >= ins_end) {
            fprintf(stderr, "Over end\n");
            break;
        }

        if (ins_cur->useless)
            continue;

        fprintf(stderr, "Try to reduce ins_cur: ");
        ins_print(ins_cur, stderr);
        fprintf(stderr, "Try to reduce ins_next: ");
        ins_print(ins_next, stderr);

        if ((ins_cur->type == MovMR) &&
            (ins_next->type == MovRM)) {
            if ((ins_cur->dest != ins_next->source) || (ins_cur->source != ins_next->dest))
                continue;
            ins_cur->useless = true;
            ins_next->useless = true;
            fprintf(stderr, "Removing a pair of Ins:\n");
            ins_print(ins_cur, stderr);
            ins_print(ins_next, stderr);
        }
    }
}

void insvector_peephole_opt(insvector* self) {
    for (size_t i = 0; i < self->size - 1; i++) {
        Ins* ins_1 = self->ptr + i;
        Ins* ins_2 = self->ptr + i + 1;

        fprintf(stderr, "Try to reduce ins_1: ");
        ins_print(ins_1, stderr);
        fprintf(stderr, "Try to reduce ins_2: ");
        ins_print(ins_2, stderr);

        if (ins_1->dest != ins_2->source)
            continue;
        if (ins_1->useless) {
            fprintf(stderr, "Skipping, already useless\n");
            continue;
        }

        if (ins_1->type == MovRR) {
            InsType legal_types[8] = { Add, Sub, Mul, Div, And, Or, Xor };
            for (size_t i = 0; i < 8; i++) {
                if (ins_2->type == legal_types[i]) {
                    fprintf(stderr, "Reducing RR-arith case\n");
                    ins_1->useless = true;
                    ins_2->source = ins_1->source;
                    ins_print(ins_2, stderr);
                }
            }
        }

        if (ins_1->type == MovRC) {
            if (ins_2->type == MovRR) {
                fprintf(stderr, "Reducing RC-RR case\n");
                ins_1->useless = true;
                ins_2->type = MovRC;
                ins_2->source = ins_1->source;
                ins_print(ins_2, stderr);
            }
        }

        if (ins_1->type == MovRM) {
            if (ins_2->type == MovRR) {
                fprintf(stderr, "Reducing RM-RR case\n");
                ins_1->useless = true;
                ins_2->type = MovRM;
                ins_2->source = ins_1->source;
                ins_print(ins_2, stderr);
            }
        }

        if (ins_1->type == MovMR) {
            fprintf(stderr, "First is MR\n");
            if (ins_2->type == MovRM) {
                fprintf(stderr, "Second is RM\n");
                if ((ins_1->dest == ins_2->source) &&
                    (ins_1->source == ins_2->dest)) {
                    fprintf(stderr, "Reducing MR-RM case\n");
                    ins_1->useless = true;
                    ins_2->useless = true;
                }
            }
        }
    }
    insvector_peephole_opt_post(self);
}

void insvector_global_init() {
    instructions = insvector_new();
}

void instruction_push(Ins ins) {
    insvector_push(&instructions, ins);
}

/* end namespace insvector */

typedef struct {
    bool occupied;
    size_t resident;
    size_t end;
} RegEntry;

typedef struct {
    bool occupied;
    size_t resident;
    size_t end;
} MemEntry;

static RegEntry regs[8];
static MemEntry mems[64];

void regs_gc(size_t ir_idx) {
    for (size_t i = 0; i < 8; i++) {
        if (regs[i].occupied && regs[i].end == ir_idx) {
            fprintf(stderr, "> Recycle r%zu; was holding %zu\n", i,
                    regs[i].resident);
            regs[i].occupied = false;
            regs[i].resident = 0;
            regs[i].end      = 0;
        }
    }
}

void mems_gc(size_t ir_idx) {
    for (size_t i = 0; i < 64; i++) {
        if (mems[i].occupied && mems[i].end == ir_idx) {
            fprintf(stderr, "> Recycle mem[%zu]; was holding %zu\n", i,
                    mems[i].resident);
            mems[i].occupied = false;
            mems[i].resident = 0;
            mems[i].end      = 0;
        }
    }
}

void regs_print() {
    for (size_t i = 0; i < 8; i++) {
        fprintf(stderr, "[%zu: (%d) %zu, %zu] ", i, regs[i].occupied,
                regs[i].resident, regs[i].end);
    }
    fputc('\n', stderr);
}

void mems_print() {
    for (size_t i = 0; i < 64; i++) {
        fprintf(stderr, "[%zu: (%d) %zu, %zu] ", i, mems[i].occupied,
                mems[i].resident, mems[i].end);
    }
    fputc('\n', stderr);
}

void machine_init() {
    *boolvector_at(&spilled, 0) = true;
    *boolvector_at(&spilled, 1) = true;
    *boolvector_at(&spilled, 2) = true;
    mems[0] = (MemEntry){true, 0, *vector_at(&curid_life_end, 0)};
    mems[1] = (MemEntry){true, 1, *vector_at(&curid_life_end, 1)};
    mems[2] = (MemEntry){true, 2, *vector_at(&curid_life_end, 2)};
}

size_t ins_find_empty_reg() {
    for (size_t i = 0; i < 8; i++) {
        if (!(regs[i].occupied)) {
            return i;
        }
    }
    return NO_EMPTY_REG;
}

size_t ins_find_reg(size_t cur) {
    for (size_t i = 0; i < 8; i++) {
        if ((regs[i].occupied) && (regs[i].resident == cur))
            return i;
    }
    return SIZE_MAX;
}

size_t ins_spill_reg_save() {
    for (size_t i = 3; i < 8; i++) {
        *boolvector_at(&spilled, regs[i].resident) = true;
        bool spill_success                         = false;
        for (size_t j = 0; j < 64; j++) {
            if (!(mems[j].occupied)) {
                mems[j].occupied = true;
                mems[j].end      = regs[i].end;
                mems[j].resident = regs[i].resident;
                fprintf(stderr, "Spilling r%zu (was holding %zu)\n", i,
                        regs[i].resident);
                instruction_push(ins_new(MovMR, j * 4, i));
                spill_success = true;
                break;
            }
        }
        if (!spill_success) {
            fprintf(stderr, "Failed to spill register; Memory is full\n");
            exit(1);
        }
        size_t reg_to_spill = i;
        return reg_to_spill;
    }
    fprintf(stderr, "Failed to spill register; Can't find suitable register\n");
    exit(1);
}

size_t ins_spill_reg(size_t sc_count, int32_t cur_1, int32_t cur_2) {
    fprintf(stderr, "Start spilling (sc_count=%zu, cur_1=%" PRId32 ", cur_2=%" PRId32")\n", sc_count, cur_1, cur_2);
    size_t max_end_time = 0;
    size_t max_end_reg  = SIZE_MAX;

    for (size_t i = 0; i < 8; i++) {
        if (sc_count >= 1 && regs[i].resident == cur_1)
            continue;
        if (sc_count >= 2 && regs[i].resident == cur_2)
            continue;
        if (regs[i].end > max_end_time) {
            max_end_reg = i;
            max_end_time = regs[i].end;
        }
    }

    if (max_end_reg != SIZE_MAX) {
        *boolvector_at(&spilled, regs[max_end_reg].resident) = true;
        bool spill_success = false;
        for (size_t j = 0; j < 64; j++) {
            if (!(mems[j].occupied)) {
                mems[j].occupied = true;
                mems[j].end      = regs[max_end_reg].end;
                mems[j].resident = regs[max_end_reg].resident;
                fprintf(stderr, "Spilling r%zu (was holding %zu)\n", max_end_reg,
                        regs[max_end_reg].resident);
                instruction_push(ins_new(MovMR, j * 4, max_end_reg));
                spill_success = true;
                break;
            }
        }
        if (!spill_success) {
            fprintf(stderr, "Failed to spill register; Memory is full\n");
            exit(1);
        }
        size_t reg_to_spill = max_end_reg;
        return reg_to_spill;
    } else {
        fprintf(stderr, "Failed to spill register; Can't find suitable register\n");
        exit(1);
    }
}

size_t ins_get_spill_address(size_t curid) {
    for (size_t i = 0; i < 64; i++) {
        if (mems[i].occupied && mems[i].resident == curid)
            return i;
    }
    fprintf(stderr, "Failed to get spilled register address for curid %zu\n",
            curid);
    mems_print();
    exit(1);
}

void ins_output_save(IR* ir, size_t ir_idx) {
    fprintf(stderr, "\nInput IR: ");
    ir_print(ir);
    size_t sc_count = 1;
    size_t sc1_reg;
    if (regs[ir->dest].occupied) {
        for (size_t j = 0; j < 64; j++) {
            if (!(mems[j].occupied)) {
                fprintf(stderr, "Spilling %" PRId32 " to mems[%zu]\n", ir->dest,
                        j);
                mems[j].occupied = true;
                mems[j].end      = regs[ir->dest].end;
                mems[j].resident = regs[ir->dest].resident;
                fprintf(stderr, "Spilling r%" PRId32 " (was holding %zu)\n",
                        ir->dest, regs[ir->dest].resident);
                instruction_push(ins_new(MovMR, j * 4, ir->dest));
                *boolvector_at(&spilled, regs[ir->dest].resident) = true;
                break;
            }
        }
    }
    regs[ir->dest].occupied = true;
    regs[ir->dest].end      = SIZE_MAX;
    regs[ir->dest].resident = ir->dest;
    if (true == *boolvector_at(&spilled, ir->sc1)) {
        size_t spill_addr = ins_get_spill_address(ir->sc1);
        size_t reg        = ins_find_empty_reg();
        if (reg == NO_EMPTY_REG) {
            sc1_reg                = ins_spill_reg_save();
            regs[sc1_reg].occupied = true;
            regs[sc1_reg].resident = ir->sc1;
            regs[sc1_reg].end      = *vector_at(&curid_life_end, ir->sc1);
        } else {
            sc1_reg = reg;
        }
        instruction_push(ins_new(MovRM, sc1_reg, spill_addr * 4));
    } else {
        sc1_reg = ins_find_reg(ir->sc1);
        fprintf(stderr, "IFR: %zu\n", sc1_reg);
        if (sc1_reg == NO_EMPTY_REG) {
            fprintf(stderr, "sc1_reg shouldn't be not found. Bug?\n");
            fprintf(stderr, "Was finding curid %" PRId32 "\n", ir->sc1);
            regs_print();
            boolvector_print(&spilled);
            exit(1);
            return;
        }
    }
    instruction_push(ins_new(MovRR, ir->dest, sc1_reg));
}

void ins_output(IR* ir, size_t ir_idx) {
    if (ir->type == IRSave)
        return ins_output_save(ir, ir_idx);
    size_t dest_reg;
    size_t sc1_reg;
    size_t sc2_reg;
    size_t sc_count = ir_get_sc_count(ir);

    fprintf(stderr, "\nInput IR: ");
    ir_print(ir);
    size_t reg = ins_find_empty_reg();

    if (reg == NO_EMPTY_REG) {
        dest_reg = ins_spill_reg(sc_count, ir->sc1, ir->sc2);
    } else {
        dest_reg = reg;
    }

    regs[dest_reg].occupied = true;
    regs[dest_reg].resident = ir->dest;
    regs[dest_reg].end      = *vector_at(&curid_life_end, ir->dest);
    if (sc_count >= 2) {
        fprintf(stderr, "sc_count = 2 case\n");
        if (true == *boolvector_at(&spilled, ir->sc1)) {
            fprintf(stderr, "%" PRId32 " is spilled, must read from memory first\n", ir->sc1);
            size_t spill_addr = ins_get_spill_address(ir->sc1);
            size_t reg        = ins_find_empty_reg();
            if (reg == NO_EMPTY_REG) {
                fprintf(stderr, "No empty register to read into, must spill to make space\n");
                sc1_reg = ins_spill_reg(sc_count, ir->dest, ir->sc2);
            } else {
                fprintf(stderr, "Has empty register %zu, using it\n", reg);
                sc1_reg = reg;
            }
            regs[sc1_reg].occupied = true;
            regs[sc1_reg].resident = ir->sc1;
            regs[sc1_reg].end      = *vector_at(&curid_life_end, ir->sc1);
            instruction_push(ins_new(MovRM, sc1_reg, spill_addr * 4));
        } else {
            sc1_reg = ins_find_reg(ir->sc1);
            if (sc1_reg == NO_EMPTY_REG) {
                fprintf(stderr, "sc1_reg shouldn't be not found. Bug?\n");
                exit(1);
            }
        }
        if (true == *boolvector_at(&spilled, ir->sc2)) {
            fprintf(stderr, "%" PRId32 " is spilled, must read from memory first\n", ir->sc2);
            size_t spill_addr = ins_get_spill_address(ir->sc2);
            size_t reg        = ins_find_empty_reg();
            if (reg == NO_EMPTY_REG) {
                fprintf(stderr, "No empty register to read into, must spill to make space\n");
                sc2_reg = ins_spill_reg(sc_count, ir->dest, ir->sc1);
            } else {
                fprintf(stderr, "Has empty register %zu, using it\n", reg);
                sc2_reg = reg;
            }
            regs[sc2_reg].occupied = true;
            regs[sc2_reg].resident = ir->sc2;
            regs[sc2_reg].end      = *vector_at(&curid_life_end, ir->sc2);
            instruction_push(ins_new(MovRM, sc2_reg, spill_addr * 4));
        } else {
            sc2_reg = ins_find_reg(ir->sc2);
            if (sc2_reg == NO_EMPTY_REG) {
                fprintf(stderr, "sc2_reg shouldn't be not found. Bug?\n");
                exit(1);
            }
        }
    } else if (sc_count >= 1) {
        if (true == *boolvector_at(&spilled, ir->sc1)) {
            size_t spill_addr = ins_get_spill_address(ir->sc1);
            size_t reg        = ins_find_empty_reg();
            if (reg == NO_EMPTY_REG) {
                sc1_reg                = ins_spill_reg(sc_count, ir->dest, 0);
            } else {
                sc1_reg = reg;
            }
            regs[sc1_reg].occupied = true;
            regs[sc1_reg].resident = ir->sc1;
            regs[sc1_reg].end      = *vector_at(&curid_life_end, ir->sc1);
            instruction_push(ins_new(MovRM, sc1_reg, spill_addr * 4));
        } else {
            sc1_reg = ins_find_reg(ir->sc1);
            if (sc1_reg == NO_EMPTY_REG) {
                fprintf(stderr, "sc1_reg shouldn't be not found. Bug?\n");
                fprintf(stderr, "Was finding curid %" PRId32 "\n", ir->sc1);
                regs_print();
                boolvector_print(&spilled);
                exit(1);
            }
        }
    }
    switch (ir->type) {
    case IRSet:
        instruction_push(ins_new(MovRC, dest_reg, ir->sc1));
        break;
    case IRAdd:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Add, dest_reg, sc2_reg));
        break;
    case IRSub:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Sub, dest_reg, sc2_reg));
        break;
    case IRMul:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Mul, dest_reg, sc2_reg));
        break;
    case IRDiv:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Div, dest_reg, sc2_reg));
        break;
    case IRAnd:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(And, dest_reg, sc2_reg));
        break;
    case IROr:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Or, dest_reg, sc2_reg));
        break;
    case IRXor:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        instruction_push(ins_new(Xor, dest_reg, sc2_reg));
        break;
    case IRMov:
        instruction_push(ins_new(MovRR, dest_reg, sc1_reg));
        break;
    }
    regs_gc(ir_idx);
    mems_gc(ir_idx);
}

void ins_output_instructions(irvector* ir_vec) {
    for (size_t i = 0; i < ir_vec->size; i++) {
        ins_output(ir_vec->ptr + i, i);
    }
    instruction_push(ins_new(Exit, 0, 0));
}

void ins_print_instructions() {
    for (size_t i = 0; i < instructions.size; i++) {
        Ins* ins = instructions.ptr + i;
        if (ins->useless)
            continue;
        ins_print(ins, stdout);
    }
}
/* end namespace ins */

int main() {
    symtbl_init();
    fprintf(stderr, "=== Lexing ===\n");
    tokenvector tk_vec = lex();
    tokenvector_print(&tk_vec);

    fprintf(stderr, "=== Building AST ===\n");
    astvector ast_vec = astvector_new();
    while (tokenvector_cur(&tk_vec) != NULL) {
        astvector_push(&ast_vec, ast_new(&tk_vec));
        if (!(astvector_back(&ast_vec)->legal)) {
            printf("EXIT 1\n");
            exit(0);
        }
    }
    for (size_t i = 0; i < ast_vec.size; i++) {
        ast_print(astvector_at(&ast_vec, i));
    }

    fprintf(stderr, "=== Performing constant folding ===\n");
    astvector_constfold(&ast_vec);
    for (size_t i = 0; i < ast_vec.size; i++) {
        ast_print(astvector_at(&ast_vec, i));
    }

    fprintf(stderr, "=== Performing dead code elimination ===\n");
    astvector_deadcode_elim(&ast_vec);
    for (size_t i = 0; i < ast_vec.size; i++) {
        ast_print(astvector_at(&ast_vec, i));
    }

    fprintf(stderr, "=== Converting AST to SSA-formed IR ===\n");
    idmap_init();
    irvector ir_vec = irvector_from_astvector(&ast_vec);
    irvector_print(&ir_vec);

    fprintf(stderr, "=== Converting IR to Ins ===\n");
    insvector_global_init();
    machine_init();
    ins_output_instructions(&ir_vec);
    
    fprintf(stderr, "=== Performing peephole optimization ===\n");
    insvector_peephole_opt(&instructions);

    fprintf(stderr, "=== Printing instructions to STDOUT ===\n");
    ins_print_instructions();
    return 0;
}

